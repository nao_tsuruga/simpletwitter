package chapter6.service;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.csv.CsvDataSetWriter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import chapter6.beans.User;
import chapter6.utils.DBUtil;

// 1. テストクラス名は任意のものに変更してください。
// 2. L.23~86は雛形として使用してください。
// 3．L.44のファイル名は各自作成したファイル名に書き換えてください。
public class UserServiceTest {

	private File file;

	@Before
	public void setUp() throws Exception {

		IDatabaseConnection connection = null;
		try {
			//(1)IDatabaseConnectionを取得
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			//(2)現状のバックアップを取得
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("users");

			file = File.createTempFile("temp", ".xml");
			FlatXmlDataSet.write(partialDataSet,new FileOutputStream(file));

			//バックアップをcsvで出力
			CsvDataSetWriter.write(partialDataSet,new File("backup"));

			//(3)テストデータを投入する
			IDataSet dataSetUser = new FlatXmlDataSet(new File("user_test_data.xml"));
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetUser);

			DBUtil.commit(conn);

			IDataSet dataSetUpdateUser = new FlatXmlDataSet(new File("user_update_test_data.xml"));
			DatabaseOperation.UPDATE.execute(connection, dataSetUpdateUser);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	@After
	public void tearDown() throws Exception {
		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			IDataSet dataSet = new FlatXmlDataSet(file);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			//一時ファイルの削除
			if (file != null) {
				file.delete();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}

		}
	}

	// 以下に課題のテストメソッドを作成
	@Test
	public void testUser() throws Exception {

		//参照メソッドの実行
		int id = 1;
		List<User> resultList = DBUtil.updateUser(id);
		//値の検証
		//件数
		assertEquals(1, resultList.size());
		//データ
		User result001 = resultList.get(0);
		assertEquals("name=DbUnitTest2", "name=" + result001.getName());


	}
}
