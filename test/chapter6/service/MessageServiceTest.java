package chapter6.service;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.csv.CsvDataSetWriter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import chapter6.beans.Message;
import chapter6.utils.DBUtil;

// 1. テストクラス名は任意のものに変更してください。
// 2. L.23~86は雛形として使用してください。
// 3．L.44のファイル名は各自作成したファイル名に書き換えてください。
public class MessageServiceTest {

	private File file;

	@Before
	public void setUp() throws Exception {

		IDatabaseConnection connection = null;
		try {
			//(1)IDatabaseConnectionを取得
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			//(2)現状のバックアップを取得
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("messages");

			file = File.createTempFile("temp", ".xml");
			FlatXmlDataSet.write(partialDataSet,new FileOutputStream(file));

			//バックアップをcsvで出力
			CsvDataSetWriter.write(partialDataSet,new File("backup"));

			//(3)テストデータを投入する
			IDataSet dataSetMessage = new FlatXmlDataSet(new File("message_test_data.xml"));
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetMessage);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	@After
	public void tearDown() throws Exception {
		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			IDataSet dataSet = new FlatXmlDataSet(file);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			//一時ファイルの削除
			if (file != null) {
				file.delete();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}

		}
	}

	// 以下に課題のテストメソッドを作成
	@Test
	public void testAllMessage() throws Exception {
		//参照メソッドの実行
		List<Message> resultList = DBUtil.allMessage();
		//値の検証
		//件数
		assertEquals(3, resultList.size());
		//データ
		Message result001 = resultList.get(0);
		assertEquals("user_id=1", "user_id=" + result001.getUserId());
		assertEquals("text=DbUnitTest1", "text=" + result001.getText());

		Message result002 = resultList.get(1);
		assertEquals("user_id=6", "user_id=" + result002.getUserId());
		assertEquals("text=DbUnitTest2", "text=" + result002.getText());

		Message result003 = resultList.get(2);
		assertEquals("user_id=32", "user_id=" + result003.getUserId());
		assertEquals("text=DbUnitTest3", "text=" + result003.getText());


		//参照メソッドの実行
		int id = 6;
		resultList = DBUtil.allUserMessage(id);
		//値の検証
		//件数
		assertEquals(1, resultList.size());
		//データ
		result001 = resultList.get(0);
		assertEquals("user_id=6", "user_id=" + result001.getUserId());
		assertEquals("text=DbUnitTest2", "text=" + result001.getText());


	}
}