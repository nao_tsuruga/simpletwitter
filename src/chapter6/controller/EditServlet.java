package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    	HttpSession session = request.getSession();
        String id = request.getParameter("id");
        Message message = null;
        if (!StringUtils.isEmpty(id) && (id.matches("^[0-9]*$"))) {
        	int messageId = Integer.parseInt(id);
            message = new MessageService().select(messageId);
            if (message != null) {
            	request.setAttribute("message", message);
                request.getRequestDispatcher("edit.jsp").forward(request, response);
            	return;
            }
        }

        List<String> errorMessages = new ArrayList<String>();
        errorMessages.add("不正なパラメータが入力されました");
    	session.setAttribute("errorMessages",errorMessages);
    	response.sendRedirect("./");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<String> errorMessages = new ArrayList<String>();
        int messageId = Integer.parseInt(request.getParameter("id"));
        Message message = new MessageService().select(messageId);
        message.setText(request.getParameter("text"));
        String text = message.getText();
		if (!isValid(text, errorMessages)) {
			message.setText(message.getText());
			request.setAttribute("message", message);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}
        new MessageService().update(message);
        response.sendRedirect("./");
    }

    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
