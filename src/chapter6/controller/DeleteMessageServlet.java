package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.service.MessageService;
@WebServlet(urlPatterns = { "/deleteMessage" })

public class DeleteMessageServlet extends HttpServlet {

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		HttpSession session = request.getSession();
		String id = request.getParameter("id");
        List<String> errorMessages = new ArrayList<String>();
        int messageId = Integer.parseInt(id);

		new MessageService().delete(messageId);

		session.setAttribute("errorMessages", errorMessages);
        response.sendRedirect("./");

    }
}
