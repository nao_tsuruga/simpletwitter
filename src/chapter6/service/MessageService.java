package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;


public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String start, String end) throws ParseException {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();
			/*
			* idをnullで初期化
			* ServletからuserIdの値が渡ってきていたら
			* 整数型に型変換し、idに代入
			*/
			Integer id = null;
			if (!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			Timestamp startDate = Timestamp.valueOf("2022-04-01 00:00:00");
			Timestamp endDate = new Timestamp(System.currentTimeMillis());

			if (!StringUtils.isEmpty(start)) {
				startDate = Timestamp.valueOf(start + " 00:00:00");
			}

			if (!StringUtils.isEmpty(end)) {
				endDate = Timestamp.valueOf(end + " 23:59:59");
			}

			/*
			* messageDao.selectに引数としてInteger型のidを追加
			* idがnullだったら全件取得する
			* idがnull以外だったら、その値に対応するユーザーIDの投稿を取得する
			*/
			List<UserMessage> messages = new UserMessageDao().select(connection, id, startDate, endDate, LIMIT_NUM);
			commit(connection);
			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void delete(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
		return;
    }

	public Message select(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            Message message = new MessageDao().select(connection, messageId);
            commit(connection);
            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void update(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}