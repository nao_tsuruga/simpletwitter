package chapter6.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.exception.SQLRuntimeException;

/**
 * DB(コネクション関係)のユーティリティー
 */
public class DBUtil {

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/simple_twitter";
	private static final String USER = "root";
	private static final String PASSWORD = "root";

	static {

		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * コネクションを取得します。
	 *
	 * @return
	 */
	public static Connection getConnection() {

		try {
			Connection connection = DriverManager.getConnection(URL, USER,
					PASSWORD);

			connection.setAutoCommit(false);

			return connection;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * コミットします。
	 *
	 * @param connection
	 */
	public static void commit(Connection connection) {

		try {
			connection.commit();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * ロールバックします。
	 *
	 * @param connection
	 */
	public static void rollback(Connection connection) {

		if (connection == null) {
			return;
		}

		try {
			connection.rollback();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	//◇◇DBUnit用追加機能①参照メソッド
	public static List<Message> allMessage() throws Exception {

		Connection connection = null;
		List<Message> messagesList = new ArrayList<Message>();

		try {
			connection = getConnection();
			String sql = "SELECT * FROM messages ORDER BY user_id";
			PreparedStatement ps = connection.prepareStatement(sql);

			ResultSet result = ps.executeQuery();

			while (result.next()) {
				Message message = new Message();
				message.setUserId(result.getInt("user_id"));
				message.setText(result.getString("text"));

				messagesList.add(message);
			}

		} finally {
			if (connection != null)
				connection.close();
		}
		return messagesList;

	}
	//◇◇DBUnit用追加機能②参照メソッド
	public static List<Message> allUserMessage(int id) throws Exception {

		Connection connection = null;
		List<Message> messagesList = new ArrayList<Message>();

		try {
			connection = getConnection();
			String sql = "SELECT * FROM messages WHERE user_id=" + id;
			PreparedStatement ps = connection.prepareStatement(sql);

			ResultSet result = ps.executeQuery();

			while (result.next()) {
				Message message = new Message();
				message.setUserId(result.getInt("user_id"));
				message.setText(result.getString("text"));

				messagesList.add(message);
			}

		} finally {
			if (connection != null)
				connection.close();
		}
		return messagesList;

	}

	//◇◇DBUnit用追加機能User参照メソッド
	public static List<User> updateUser(int id) throws Exception {

		Connection connection = null;
		List<User> usersList = new ArrayList<User>();

		try {
			connection = getConnection();
			String sql = "SELECT * FROM users WHERE id=" + id;
			PreparedStatement ps = connection.prepareStatement(sql);

			ResultSet result = ps.executeQuery();

			while (result.next()) {
				User user = new User();
				user.setAccount(result.getString("account"));
				user.setName(result.getString("name"));

				usersList.add(user);
			}

		} finally {
			if (connection != null)
				connection.close();
		}
		return usersList;

	}
}
